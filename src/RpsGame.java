//Mikael Baril 1844064
import java.util.*;
public class RpsGame {

	private int wins;
	private int ties;
	private int losses;
	Random ran = new Random();
	
	
	/*this.losses = losses;
	this.wins = wins ;
	this.ties = ties;
	*/
	public int getWins() {
		
		return this.wins;
		
	}
	public int getTies() {
	
		return this.ties;
		
	}
	public int getLosses() {
		
		return this.losses;
	
	
	}
	public String playRound(String pChoice) {
		String choice;
		int num = ran.nextInt(3);
		if (num==0) {
			choice = "rock";
		}
		else if (num==1) {
			choice = "paper";
		}
		else {
			choice = "scissors";
		}
		
		if(pChoice.equals("rock")&& choice.equals("rock")) {
			this.ties++;
			return "The computer played: " + choice + "It's a tie";
		}
		else if(pChoice.equals("rock")&& choice.equals("paper")) {
			this.losses++;
			return "The computer played: " + choice + "It's a loss :(";
		}
		else if(pChoice.equals("rock")&& choice.equals("scissors")) {
			this.wins++;
			return "The computer played: " + choice + "It's a win! Good Job.";
		}
		else if(pChoice.equals("paper")&& choice.equals("rock")) {
			this.wins++;
			return "The computer played: " + choice + "It's a win! Good Job";
		}
		else if(pChoice.equals("paper")&& choice.equals("paper")) {
			this.ties++;
			return "The computer played: " + choice + "It's a tie";
		}
		else if(pChoice.equals("paper")&& choice.equals("scissors")) {
			this.losses++;
			return "The computer played: " + choice + "It's a loss :(";
		}
		else if(pChoice.equals("scissors")&& choice.equals("scissors")) {
			this.ties++;
			return "The computer played: " + choice + "It's a tie ";
		}
		else if(pChoice.equals("scissors")&& choice.equals("rock")) {
			this.losses++;
			return "The computer played: " + choice + "It's a loss :(";
		}
		else{
			this.wins++;
			return "The computer played: " + choice + " It's a win! Good Job" ;
		}
	}
}
