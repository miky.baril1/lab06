//Mikael Baril 1844064
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsChoice implements EventHandler<ActionEvent> {
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private RpsGame round;
	private String pChoice;
	
	public void handle(ActionEvent evt) {
		
		this.wins.setText("You have " + this.round.getWins() + "wins.");
		this.losses.setText("You have " + this.round.getLosses() + "losses.");
		this.ties.setText("You have " + this.round.getTies() + "ties");
	}
	
	public RpsChoice(Textfield wins,Textfield losses,Textfield ties, RpsGame round, String pChoice )
	{
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.round = round;
		this.pChoice = pChoice;
		
	}
	
	
	

}
