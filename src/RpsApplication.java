//Mikael Baril 1844064
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application{

	public void start(Stage stage) {
		Group root = new Group();
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650,300);
		scene.setFill(Color.BLACK);
		
		//Associate scene to stage and show
		stage.setTitle("Rock Paper Scissors");
		stage.setScene(scene);
		
		stage.show();
		
	}
	
	HBox buttons = new HBox();
	Button rock = new Button("rock");
	Button paper = new Button("paper");
	Button scissors = new Button("scissors");
	buttons.getChildren().addAll(rock,paper,scissors)
	
	HBox textfields = new HBox();
	TextField losses = new TextField();
	TextField ties = new TextField();
	TextField wins = new TextField();
	
	textfields.getChildren.addAll(losses,wins,ties);
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
}
